package amo

import (
	"strings"
	"encoding/json"
	"net/url"
)

func (c *Client) getCurrent(params []string) (err error) {
	u := url.URL{Path: "/api/v2/account"}
	q := u.Query()
	q.Add("with", strings.Join(params, ","))
	u.RawQuery = q.Encode()

	var resp Current
	data, err := c.getRequest(u, nil)
	if err != nil {
		return
	}
	json.Unmarshal(data, &resp)

	c.current = &resp
	return
}

func (c *Client) Current() *Current {
	return c.current
}