package amo

import (
	"net/http"
	"net/url"
	"io/ioutil"
	"strings"
	"net/http/cookiejar"
	"golang.org/x/net/publicsuffix"
	"encoding/json"
)

type Auth struct {
	Response struct {
		ErrorCode  string `json:"error_code"`
		Error      string `json:"error"`
		Ip         string `json:"ip"`
		Domain     string `json:"domain"`
		Auth       bool   `json:"auth"`
		ServerTime int64  `json:"server_time"`
	} `json:"response"`
}

type Client struct {
	client  *http.Client
	current *Current
	config
}

type config struct {
	login  string
	domain string
	hash   string
}

func NewClient(login string, domain string, hash string) (client *Client, err error) {
	config := config{login: login, domain: domain, hash: hash}

	jar, err := cookiejar.New(&cookiejar.Options{PublicSuffixList: publicsuffix.List})
	if err != nil {
		return
	}

	httpClient := &http.Client{
		Jar: jar,
	}

	client = &Client{client: httpClient, config: config}
	err = client.auth()
	if err != nil {
		return
	}
	err = client.getCurrent([]string{
		"custom_fields",
		"users",
		"pipelines",
		"groups",
		"note_types",
		"task_types",
	})
	return
}

func (c *Client) auth() (err error) {
	var u url.URL
	u.Scheme = "https"
	u.Host = c.domain + ".amocrm.ru"
	u.Path = "/private/api/auth.php"
	u.RawQuery = "type=json"
	data, err := c.postRequest(u, url.Values{"USER_LOGIN": {c.login}, "USER_HASH": {c.hash}})
	if err != nil {
		return
	}
	var a Auth
	err = json.Unmarshal(data, &a)
	if err != nil {
		return
	}
	if !a.Response.Auth {
		return &ErrA{10, "Не удалось авторизоваться, проверьте настройки сервиса"}
	}
	return
}

func (c *Client) getRequest(u url.URL, params url.Values) (data []byte, err error) {
	u.Scheme = "https"
	u.Host = c.domain + ".amocrm.ru"

	q := u.Query()
	for key, values := range params {
		q.Add(key, strings.Join(values, ","))
	}
	u.RawQuery = q.Encode()

	resp, err := c.client.Get(u.String())
	if err != nil {
		return
	}
	defer resp.Body.Close()
	data, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}

	return
}

func (c *Client) postRequest(u url.URL, params url.Values) (data []byte, err error) {
	u.Scheme = "https"
	u.Host = c.domain + ".amocrm.ru"

	resp, err := c.client.PostForm(u.String(), params)
	if err != nil {
		return
	}
	defer resp.Body.Close()
	data, err = ioutil.ReadAll(resp.Body)
	if err != nil {
		return
	}
	return
}
