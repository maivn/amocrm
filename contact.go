package amo

import (
	"net/url"
	"github.com/json-iterator/go"
)

func (c *Client) ContactList(params map[string]interface{}) (contacts *Contacts, err error) {
	u := url.URL{Path: "/api/v2/contacts"}
	u.RawQuery = paramsToQuery(params).Encode()

	var resp ResponseContactList
	data, err := c.getRequest(u, nil)
	if err != nil {
		return
	}

	jsoniter.Unmarshal(data, &resp)

	contacts = &resp.Embedded.Items
	return
}

func (contacts *Contacts) First() (has bool, contact *Contact) {
	if len(*contacts) > 0 {
		has = true
		contact = &(*contacts)[0]
		return
	}
	return
}
