package amo

func (c *Current) GetCodeTypeCustomFieldById(id int64) (code int64) {
	currentCustomFields := c.Embedded.CustomFields
	for _, customField := range currentCustomFields.Leads {
		if customField.Id == id {
			return customField.FieldType
		}
	}
	for _, customField := range currentCustomFields.Contacts {
		if customField.Id == id {
			return customField.FieldType
		}
	}
	for _, customField := range currentCustomFields.Companies {
		if customField.Id == id {
			return customField.FieldType
		}
	}
	return
}
