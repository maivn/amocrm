package amo

type ErrA struct {
	Code    int    `json:"code"`
	Message string `json:"message"`
}

func (e *ErrA) Error() string {
	return e.Message
}
