package amo

import (
	"net/url"
	"github.com/json-iterator/go"
)

func (c *Client) LeadList(params map[string]interface{}) (leads *Leads, err error) {
	u := url.URL{Path: "/api/v2/leads"}
	u.RawQuery = paramsToQuery(params).Encode()

	var resp ResponseLeadList
	data, err := c.getRequest(u, nil)
	if err != nil {
		return
	}

	jsoniter.Unmarshal(data, &resp)

	leads = &resp.Embedded.Items
	return
}

func (leads *Leads) First() (has bool, lead *Lead) {
	if len(*leads) > 0 {
		has = true
		lead = &(*leads)[0]
		return
	}
	return
}
