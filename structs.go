package amo

type Current struct {
	Id       int64             `json:"id"`
	Name     string          `json:"name"`
	Embedded CurrentEmbedded `json:"_embedded"`
}

type CurrentEmbedded struct {
	Users        Users
	CustomFields CurrentCustomFields `json:"custom_fields"`
}

type CurrentCustomFields struct {
	Contacts  CurrentContacts  `json:"contacts"`
	Leads     CurrentLeads     `json:"leads"`
	Companies CurrentCompanies `json:"companies"`
}

type CurrentContacts map[string]CurrentCustomField
type CurrentLeads map[string]CurrentCustomField
type CurrentCompanies map[string]CurrentCustomField

type CurrentCustomField struct {
	Id        int64
	Name      string
	FieldType int64 `json:"field_type"`
}

type Users map[int64]User

type User struct {
	//todo
}

type ResponseLeadList struct {
	Links    Links                `json:"_links"`
	Embedded ResponseLeadEmbedded `json:"_embedded"`
}

type ResponseLeadEmbedded struct {
	Items Leads
}

type Leads []Lead

type Lead struct {
	Id                int64          `json:"id"`
	Name              string       `json:"name"`
	ResponsibleUserId int64          `json:"responsible_user_id"`
	CreatedBy         int64          `json:"created_by"`
	CreatedAt         int64        `json:"created_at"`
	UpdatedAt         int64        `json:"updated_at"`
	AccountId         int64          `json:"account_id"`
	IsDeleted         bool         `json:"is_deleted"`
	MainContact       Path         `json:"main_contact"`
	GroupId           int64          `json:"group_id"`
	Company           Path         `json:"company"`
	ClosedAt          int64        `json:"closed_at"`
	ClosestTaskAt     int64        `json:"closest_task_at"`
	Tags              Tags         `json:"tags"`
	CustomFields      CustomFields `json:"custom_fields"`
	Contacts          Paths        `json:"contacts"`
	StatusId          int64          `json:"status_id"`
	Sale              int64          `json:"sale"`
	Pipeline          Path         `json:"pipeline"`
	LossReasonId      int64          `json:"loss_reason_id"`
	Links             Links        `json:"_links"`
}

type ResponseContactList struct {
	Links    Links                   `json:"_links"`
	Embedded ResponseContactEmbedded `json:"_embedded"`
}

type ResponseContactEmbedded struct {
	Items Contacts
}

type Contacts []Contact

type Contact struct {
	Id                int64          `json:"id"`
	Name              string       `json:"name"`
	ResponsibleUserId int64          `json:"responsible_user_id"`
	CreatedBy         int64          `json:"created_by"`
	CreatedAt         int64        `json:"created_at"`
	UpdatedAt         int64        `json:"updated_at"`
	AccountId         int64          `json:"account_id"`
	UpdatedBy         int64          `json:"updated_by"`
	GroupId           int64          `json:"group_id"`
	Company           Path         `json:"company"`
	Leads             Paths        `json:"leads"`
	ClosestTaskAt     int64        `json:"closest_task_at"`
	Tags              Tags         `json:"tags"`
	CustomFields      CustomFields `json:"custom_fields"`
	Customer          Paths        `json:"customers"`
	Links             Links        `json:"_links"`
}

type Links struct {
	Self Self
}

type Self struct {
	Href   string
	Method string
}

type Path struct {
	Id    int64    `json:"id"`
	Name  string `json:"name"`
	Links Links  `json:"_links"`
}

type Paths struct {
	Id    []int64  `json:"id"`
	Name  string `json:"name"`
	Links Links  `json:"_links"`
}

type Tags []Tag

type Tag struct {
	Id   int64    `json:"id"`
	Name string `json:"name"`
}

type CustomFields []CustomField

type CustomField struct {
	Id       int64  `json:"id"`
	Name     string `json:"name"`
	Values   Values `json:"values"`
	IsSystem bool   `json:"is_system"`
}

type Values []Value

type Value struct {
	Value   string `json:"value"`
	Enum    string `json:"enum"`
	Subtype string `json:"subtype"`
}
