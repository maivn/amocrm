package amo

func (c *Client) GetUserById(id int64) *User {
	if user, ok := c.current.Embedded.Users[id]; ok {
		return &user
	}
	return nil
}
