package amo

import (
	"strconv"
	"strings"
	"net/url"
)

func paramsToQuery(params map[string]interface{}) url.Values {
	var u url.URL
	q := u.Query()
	for key, values := range params {
		switch v := values.(type) {
		case int:
			q.Add(key, strconv.Itoa(v))
		case string:
			q.Add(key, v)
		case []string:
			q.Add(key, strings.Join(v, ","))
		}
	}
	return q
}
